import { useState } from "react";
import { useDispatch } from "react-redux";
import addDigimonsThunk from "../../store/modules/digimons/thunks";

const Search = () => {
  const [input, setInput] = useState("");
  const [error, setError] = useState(false);

  const dispatch = useDispatch();

  const handleSearch = (inputValue) => {
    setError(false);
    dispatch(addDigimonsThunk(inputValue));
    setInput("");
  };

  return (
    <div>
      <h2>Procure pelo seu Digimon!</h2>
      <div>
        <input
          value={input}
          onChange={(e) => setInput(e.target.value)}
          placeholder="Procure se Digimon"
        />
        <button onClick={() => handleSearch(input)}>Pesquisar</button>
      </div>
    </div>
  );
};

export default Search;
