import { useSelector } from "react-redux";

const Digimons = () => {
  const { digimons } = useSelector((store) => store);

  return (
    <div>
      <ul>
        {digimons &&
          digimons.map((digimon, index) => <li key={index}>{digimon.name}</li>)}
      </ul>
    </div>
  );
};

export default Digimons;
