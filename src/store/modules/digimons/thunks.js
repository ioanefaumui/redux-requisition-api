import axios from "axios";

import { addDigimon } from "./actions";

const addDigimonsThunk = (digimons) => {
  return (dispatch, getStore) => {
    axios
      .get("https://digimon-api.vercel.app/api/digimon")
      .then((response) =>
        dispatch(
          addDigimon(
            response.data.filter((item) => item.name.includes(digimons))
          )
        )
      );
  };
};

export default addDigimonsThunk;
