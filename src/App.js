import "./App.css";
import Search from "./components/Search";
import Digimons from "./components/Digimons";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>Api redux</h1>
        <Search />
        <Digimons />
      </header>
    </div>
  );
}

export default App;
